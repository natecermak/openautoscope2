# OpenAutoScope2

This repository contains details on how to build a lowish-magnification (5.55x) simultaneous brightfield/epifluorescence microscope with
a computer-controlled stage for a reasonably low cost ($3000 USD). This microscope can be used to simultaneously monitor animal behavior
via brightfield imaging and fluorescent markers (e.g. GCaMPs). The software is capable of tracking animals (particularly C. elegans) via
either the brightfield or fluorescent channels)

It contains:

1. A complete parts list to build the openAutoScope2
2. Labview code to interface with the control PCB and control the cameras.


## How does openAutoScope2 differ from openAutoScope?
- Two cameras provide simultaneous multichannel imaging
- Fluorescence channel is optimized for GFP imaging
- Software is updated to enable control of two cameras and tracking on either camera.


## Getting started
To get started, take a look at the openAutoScope construction tutorial and the parts list.

## Specifications
- Brightfield field of view is 1273 x 955 um with 10X NA 0.25 objective.
- Image resolution is 2048 x 1536 pixels.
- Up to 55 frames per second
- For fluorescence, uses 470nm LED as excitation (not flat - computational flat-field correction required).
- For brightfield 
- Microscope moves in X and Y, while stage only moves in Z (vertically).
- Typical stage speed 1.25mm/s.
- Stage movement resolution is 1.25 um.
- Travel range is approximately 115 mm in each dimension.
- Stage movement,  LED are all software-controllable.
